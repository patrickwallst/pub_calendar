drop table if exists post;




/*      Post        */
CREATE TABLE post 
( 
	slug varchar(255) primary key, 
    side int(11) ,
     pubDate varchar(255), 
     month int(11),
      vertical varchar(255), 
       parentid int(11)
);

LOAD DATA LOCAL INFILE 'C:\\Temp\\post.csv' INTO TABLE post  
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED BY '\n'

( `slug`,
  `side` ,
  `pubDate` ,
  `month`,
  `vertical` ,
  `parentid` )
;

select * from post;