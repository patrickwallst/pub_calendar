CREATE DATABASE pubcalendar;

use pubcalendar;



/*    Parent       */

CREATE TABLE parent
( 
	parentid int not null primary key, 
    side0 varchar(255) ,
     side1 varchar(255), 
      entity varchar(255), 
       universe varchar(255)
);

LOAD DATA LOCAL INFILE 'C:\\Temp\\Parent.csv' INTO TABLE parent 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED BY '\n'
IGNORE 1 LINES

( `parentid`,
  `side0` ,
  `side1` ,
  `entity` ,
  `universe` )
;

select * from parent;


/*      Post        */
CREATE TABLE post 
( 
	slug varchar(255) primary key, 
    side int(11) ,
     pubDate varchar(255), 
      vertical varchar(255), 
       parentid int(11)
);

LOAD DATA LOCAL INFILE 'C:\\Temp\\post.csv' INTO TABLE post  
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED BY '\n'
IGNORE 1 LINES

( `slug`,
  `side` ,
  `pubDate` ,
  `vertical` ,
  `parentid` )
;


select * from post;

alter table post add constraint fk_parentid foreign key (parentid) references parent(parentid);


/*       GADATA TABLE           */
drop table if exists gadata;
CREATE TABLE gadata 
( 
	`date` date ,
    `device` varchar(30) ,
	`channel` varchar(30) ,
	`source` varchar(100) ,
	`campaign` varchar(30) ,
    `slug` varchar(255) ,
	`sessionDuration` int(11) ,
	`pageviews`int(11) ,
    `sessions` int(11) ,
	`newUsers` int(11) ,
    `bounces` int(11) ,
	`uniques` int(11) ,
    `bounceRate` decimal(9,4) ,
	`pctNewUsers` decimal(9,4),
    `pageviewsPerVist` decimal(9,4),
    `avgSessionDuration` decimal(9,4),
    `pctUnique` decimal(9,4),
    index `unique_index` (`date`, `device`, `channel`, `source`, `campaign`, `slug`)
);

LOAD DATA LOCAL INFILE 'C:\\Temp\\gadata.csv' INTO TABLE gadata  
FIELDS TERMINATED BY ',' 
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES

(	@var1 ,
	`device`  ,
	`channel`  ,
	`source` ,
	`campaign`  ,
    `slug` ,
	`sessionDuration`  ,
	`pageviews` ,
    `sessions`  ,
	`newUsers`  ,
    `bounces`  ,
	`uniques`  ,
    `bounceRate`  ,
	`pctNewUsers` ,
    `pageviewsPerVist`,
    `avgSessionDuration` ,
    `pctUnique` )
SET `date` = str_to_date(@var1, '%m/%d/%Y')

;


select * from gadata;



/*      MSNDATA TABLE         */
CREATE TABLE msndata 
( 
	`date` date  ,
    `slug` varchar(255) ,
     `uniqueUsers` int, 
      `pageviews` int, 
       `pageviewsPerSession` int(11),
	primary key (`date`, `slug`)
);

CREATE TABLE msndata_raw 
( 
	`date` varchar(255) ,
	`slug` varchar(255) ,
	`uniqueUsers` varchar(255) ,
	`pageviews` varchar(255) ,
	`pageviewsPerSession` varchar(255) ,
	primary key (`date`, `slug`)
);

LOAD DATA LOCAL INFILE 'C:\\Temp\\msndata.csv' INTO TABLE msndata_raw  
FIELDS TERMINATED BY ',' 
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

delete from msndata;

insert msndata 
select 
	str_to_date(`date`, '%m/%d/%Y') as `date`,
    `slug`,
    CAST(replace( ifnull( `uniqueUsers`, 0 ), ',', '' ) AS UNSIGNED) as uniqueUsers,
    CAST(replace( ifnull( `pageviews` , 0 ), ',', '' )AS UNSIGNED) as pageviews ,
    CAST(replace( ifnull( `pageviewsPerSession` , 0 ), ',', '' ) AS decimal(32,10)) as pageviewsPerSession 
from msndata_raw;





/*              AUTHORS TABLE                    */
CREATE TABLE authors 
( 
	`wpid` int not null primary key, 
	`order` int(11),
    `author` varchar(255)
         
);

LOAD DATA LOCAL INFILE 'C:\\Temp\\authors.csv' INTO TABLE authors
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(`wpid`,`order`,`author`);




