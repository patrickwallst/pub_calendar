<?php if ( !defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class msn extends CI_Controller {

	function __construct() {
		parent::__construct();
		 $this->load->model('table_model', 'tm');
	} // end __construct()


	function index() {
    // if( $this->session->userdata( 'logged_in' ) ) 
    // {
		// $session_data = $this->session->userdata( 'logged_in' );
		// $data = array(
		// 	'username' => $session_data['username'],
		// 	'title' => $session_data['title'],
		// 	'isSeller' => $session_data['isSeller'],
		// 	'names' => $this->ddt->getsellers(),
		// 	'sysdate' => date( 'm/d/Y', strtotime( $this->ddt->totdate('pmp') ) ),
		// 	'header_name' => $this->ddt->SellerAdvertiser()
		// );

		// if(preg_match('/^DB_/i',strtoupper($session_data['username']))) {
		// 	header( "Location: " . base_url('salespersonPreview.html') );
		// 	exit();
        // }
		$data = array();
        
		$this->load->view( 'header', $data );
		$this->load->view( 'filter', $data );
		$this->load->view( 'table', $data );
		// $this->load->view( 'footer', $data );
    // } 
    // else 
    // {
	// 	// If no session, redirect to login page
	// 	header( "Location: " . base_url() );
	// }
	} // end index()

	// function logout() {
	// 	$this->session->unset_userdata( 'logged_in' );
	// 	session_destroy();
	// 	header( "Location: " . base_url() );
	// } // end logout()




	public function metrics($id = 0){

		echo $id;

		echo $this->input->get('begin_date', TRUE);

		$distinctVerticals = $this->tm->getVerticals();
		echo  json_encode($distinctVerticals);
	}
	
	
}
