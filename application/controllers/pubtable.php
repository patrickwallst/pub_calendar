<?php if ( !defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Pubtable extends CI_Controller {

	function __construct() {
		parent::__construct();
		 $this->load->model('pubtable_model', 'tm');
		 $this->load->library( 'tools' );
	} // end __construct()


	function index() {
    // if( $this->session->userdata( 'logged_in' ) ) 
    // {
		// $session_data = $this->session->userdata( 'logged_in' );
		// $data = array(
		// 	'username' => $session_data['username'],
		// 	'title' => $session_data['title'],
		// 	'isSeller' => $session_data['isSeller'],
		// 	'names' => $this->ddt->getsellers(),
		// 	'sysdate' => date( 'm/d/Y', strtotime( $this->ddt->totdate('pmp') ) ),
		// 	'header_name' => $this->ddt->SellerAdvertiser()
		// );

		// if(preg_match('/^DB_/i',strtoupper($session_data['username']))) {
		// 	header( "Location: " . base_url('salespersonPreview.html') );
		// 	exit();
        // }
		$data = array(
			'verticals' => $this->getVerticals()
		);
        
		$this->load->view( 'header', $data );
		$this->load->view( 'filter', $data );
		$this->load->view( 'pubtable', $data );
		// $this->load->view( 'footer', $data );
    // } 
    // else 
    // {
	// 	// If no session, redirect to login page
	// 	header( "Location: " . base_url() );
	// }
	} // end index()

	public function getVerticals() {
		$distinctVerticals = $this->tm->getVerticals();
		$verticalArray = array();
		foreach ($distinctVerticals as $value) {
			$verticalArray[] = $value['vertical'];
		}
		return $verticalArray;
	}


	public function getTableData( $vertical, $start_month, $end_month ) {
		$vertical = addslashes( urldecode( $vertical ) );
		$slugMetadata = $this->tm->getTrafficAcquisitionMetrics( $vertical, $start_month, $end_month );
		// $this->tools->message( $slugMetadata, 1 );

		///sorting start
		$output = array();

		foreach ($slugMetadata as $key => $value) {
			
			// $this->tools->message( $value['parentid'], 1 );

			//need to separate these values with a conditional if on one side or other
			$post_parentid = (string) $value['parentid']; //array key
			$post_url_slug = (string) $value['slug'];
			$post_side = (int) $value['side'];
			$post_pubdate = (string) $value['pubDate'];

			$post_vertical = (float) $value['vertical'];

			$ga_pageviews = (float) $value['gapageviews'];
			$ga_sessions = (float) $value['sessions']; // just labeled as sessions currently
			$msn_pageviews = (float) $value['msnpageviews'];
			$msn_sessions = (float) $value['msnsessions'];
			$ta_aol_pageviews = (float) $value['TA_AOL_pageviews'];
			$ta_aol_sessions = (float) $value['TA_AOL_sessions'];
			$ta_keywee_pageviews = (float) $value['TA_keywee_pageviews'];
			$ta_keywee_sessions = (float) $value['TA_keywee_sessions'];
			$ta_internal_pageviews = (float) $value['TA_internal_pageviews'];
			$ta_internal_sessions = (float) $value['TA_internal_sessions'];

			$superlative_side0 = trim( $value['side0'] );
			$superlative_side1 = trim( $value['side1'] );
			$superlative_side2 = $superlative_side0 . ' | ' . $superlative_side1;

			$post_entity = trim( $value['entity'] );
			$post_universe = trim( $value['universe'] );

			// Using this to create the parent title for now
			$parent_title_merge = $superlative_side0 . ' | ' . $superlative_side1 . ' - ' . $post_entity . ' | ' . $post_universe;
			// Using this for post headline for now
			$post_title_from_slug = ucwords( str_replace("-", " ", $post_url_slug) );

			// Set up Array Skeleton if not set -> will eventually make this a constructor class
			if ( ! isset($output[$value['parentid']] ) ) {
				$output[$value['parentid']] = array(
					'parentName' => $parent_title_merge,
					'parentId' => $post_parentid,
					'pubdate' => '',
					'totals' => array(
						'GA' => array(
							'Pageviews' => '',
							'Sessions' => '',
						),
						'MSN' => array(
							'Pageviews' => '',
							'Sessions' => '',
						),
						'AOL' => array(
							'Pageviews' => '',
							'Sessions' => '',
						),
						'Keywee' => array(
							'Pageviews' => '',
							'Sessions' => '',
						),
						'Internal' => array(
							'Pageviews' => '',
							'Sessions' => '',
						),
					),
					'superlative' => array(
						'0' => array(
							'side' => $superlative_side0,
							'pubdate' => '',
							'totals' => array(
								'GA' => array(
									'Pageviews' => '',
									'Sessions' => '',
								),
								'MSN' => array(
									'Pageviews' => '',
									'Sessions' => '',
								),
								'AOL' => array(
									'Pageviews' => '',
									'Sessions' => '',
								),
								'Keywee' => array(
									'Pageviews' => '',
									'Sessions' => '',
								),
								'Internal' => array(
									'Pageviews' => '',
									'Sessions' => '',
								),
							),
						),
						'1' => array(
							'side' => $superlative_side1,
							'pubdate' => '',
							'totals' => array(
								'GA' => array(
									'Pageviews' => '',
									'Sessions' => '',
								),
								'MSN' => array(
									'Pageviews' => '',
									'Sessions' => '',
								),
								'AOL' => array(
									'Pageviews' => '',
									'Sessions' => '',
								),
								'Keywee' => array(
									'Pageviews' => '',
									'Sessions' => '',
								),
								'Internal' => array(
									'Pageviews' => '',
									'Sessions' => '',
								),
							),
						),
						'2' => array(
							'side' => $superlative_side2,
							'pubdate' => '',
							'totals' => array(
								'GA' => array(
									'Pageviews' => '',
									'Sessions' => '',
								),
								'MSN' => array(
									'Pageviews' => '',
									'Sessions' => '',
								),
								'AOL' => array(
									'Pageviews' => '',
									'Sessions' => '',
								),
								'Keywee' => array(
									'Pageviews' => '',
									'Sessions' => '',
								),
								'Internal' => array(
									'Pageviews' => '',
									'Sessions' => '',
								),
							),
						),
					),
				);
			} //end of skeleton

			//Totals - Pubdate and Metrics
			$output[$value['parentid']]['pubdate'] =
			isset( $output[$value['parentid']]['pubdate'] ) ? 
			max( $output[$value['parentid']]['pubdate'], $post_pubdate ) : $post_pubdate;

			$output[$value['parentid']]['totals']['GA']['Pageviews'] =
			isset( $output[$value['parentid']]['totals']['GA']['Pageviews'] ) ? 
			(float) $output[$value['parentid']]['totals']['GA']['Pageviews'] + (float) $ga_pageviews : $ga_pageviews;

			$output[$value['parentid']]['totals']['GA']['Sessions'] =
			isset( $output[$value['parentid']]['totals']['GA']['Sessions'] ) ? 
			(float) $output[$value['parentid']]['totals']['GA']['Sessions'] + (float) $ga_sessions : $ga_sessions;

			$output[$value['parentid']]['totals']['MSN']['Pageviews'] =
			isset( $output[$value['parentid']]['totals']['MSN']['Pageviews'] ) ? 
			(float) $output[$value['parentid']]['totals']['MSN']['Pageviews'] + (float) $msn_pageviews : $msn_pageviews;

			$output[$value['parentid']]['totals']['MSN']['Sessions'] =
			isset( $output[$value['parentid']]['totals']['MSN']['Sessions'] ) ? 
			(float) $output[$value['parentid']]['totals']['MSN']['Sessions'] + (float) $msn_sessions : $msn_sessions;

			$output[$value['parentid']]['totals']['AOL']['Pageviews'] =
			isset( $output[$value['parentid']]['totals']['AOL']['Pageviews'] ) ? 
			(float) $output[$value['parentid']]['totals']['AOL']['Pageviews'] + (float) $ta_aol_pageviews : $ta_aol_pageviews;

			$output[$value['parentid']]['totals']['AOL']['Sessions'] =
			isset( $output[$value['parentid']]['totals']['AOL']['Sessions'] ) ? 
			(float) $output[$value['parentid']]['totals']['AOL']['Sessions'] + (float) $ta_aol_sessions : $ta_aol_sessions;

			$output[$value['parentid']]['totals']['Keywee']['Pageviews'] =
			isset( $output[$value['parentid']]['totals']['Keywee']['Pageviews'] ) ? 
			(float) $output[$value['parentid']]['totals']['Keywee']['Pageviews'] + (float) $ta_keywee_pageviews : $ta_keywee_pageviews;

			$output[$value['parentid']]['totals']['Keywee']['Sessions'] =
			isset( $output[$value['parentid']]['totals']['Keywee']['Sessions'] ) ? 
			(float) $output[$value['parentid']]['totals']['Keywee']['Sessions'] + (float) $ta_keywee_sessions : $ta_keywee_sessions;

			$output[$value['parentid']]['totals']['Internal']['Pageviews'] =
			isset( $output[$value['parentid']]['totals']['Internal']['Pageviews'] ) ? 
			(float) $output[$value['parentid']]['totals']['Internal']['Pageviews'] + (float) $ta_internal_pageviews : $ta_internal_pageviews;

			$output[$value['parentid']]['totals']['Internal']['Sessions'] =
			isset( $output[$value['parentid']]['totals']['Internal']['Sessions'] ) ? 
			(float) $output[$value['parentid']]['totals']['Internal']['Sessions'] + (float) $ta_internal_sessions : $ta_internal_sessions;

			//Superlative Pubdate and Totals
			$output[$value['parentid']]['superlative'][$post_side]['pubdate'] =
			isset( $output[$value['parentid']]['superlative'][$post_side]['pubdate'] ) ? 
			max( $output[$value['parentid']]['superlative'][$post_side]['pubdate'], $post_pubdate ) : $post_pubdate;
			
			$output[$value['parentid']]['superlative'][$post_side]['pubdate'] =
			isset( $output[$value['parentid']]['superlative'][$post_side]['pubdate'] ) ? 
			max( $output[$value['parentid']]['superlative'][$post_side]['pubdate'], $post_pubdate ) : $post_pubdate;

			$output[$value['parentid']]['superlative'][$post_side]['totals']['GA']['Pageviews'] =
			isset( $output[$value['parentid']]['superlative'][$post_side]['totals']['GA']['Pageviews'] ) ? 
			(float) $output[$value['parentid']]['superlative'][$post_side]['totals']['GA']['Pageviews'] + (float) $ga_pageviews : $ga_pageviews;

			$output[$value['parentid']]['superlative'][$post_side]['totals']['GA']['Sessions'] =
			isset( $output[$value['parentid']]['superlative'][$post_side]['totals']['GA']['Sessions'] ) ? 
			(float) $output[$value['parentid']]['superlative'][$post_side]['totals']['GA']['Sessions'] + (float) $ga_sessions : $ga_sessions;

			$output[$value['parentid']]['superlative'][$post_side]['totals']['MSN']['Pageviews'] =
			isset( $output[$value['parentid']]['superlative'][$post_side]['totals']['MSN']['Pageviews'] ) ? 
			(float) $output[$value['parentid']]['superlative'][$post_side]['totals']['MSN']['Pageviews'] + (float) $msn_pageviews : $msn_pageviews;

			$output[$value['parentid']]['superlative'][$post_side]['totals']['MSN']['Sessions'] =
			isset( $output[$value['parentid']]['superlative'][$post_side]['totals']['MSN']['Sessions'] ) ? 
			(float) $output[$value['parentid']]['superlative'][$post_side]['totals']['MSN']['Sessions'] + (float) $msn_sessions : $msn_sessions;

			$output[$value['parentid']]['superlative'][$post_side]['totals']['AOL']['Pageviews'] =
			isset( $output[$value['parentid']]['superlative'][$post_side]['totals']['AOL']['Pageviews'] ) ? 
			(float) $output[$value['parentid']]['superlative'][$post_side]['totals']['AOL']['Pageviews'] + (float) $ta_aol_pageviews : $ta_aol_pageviews;

			$output[$value['parentid']]['superlative'][$post_side]['totals']['AOL']['Sessions'] =
			isset( $output[$value['parentid']]['superlative'][$post_side]['totals']['AOL']['Sessions'] ) ? 
			(float) $output[$value['parentid']]['superlative'][$post_side]['totals']['AOL']['Sessions'] + (float) $ta_aol_sessions : $ta_aol_sessions;

			$output[$value['parentid']]['superlative'][$post_side]['totals']['Keywee']['Pageviews'] =
			isset( $output[$value['parentid']]['superlative'][$post_side]['totals']['Keywee']['Pageviews'] ) ? 
			(float) $output[$value['parentid']]['superlative'][$post_side]['totals']['Keywee']['Pageviews'] + (float) $ta_keywee_pageviews : $ta_keywee_pageviews;

			$output[$value['parentid']]['superlative'][$post_side]['totals']['Keywee']['Sessions'] =
			isset( $output[$value['parentid']]['superlative'][$post_side]['totals']['Keywee']['Sessions'] ) ? 
			(float) $output[$value['parentid']]['superlative'][$post_side]['totals']['Keywee']['Sessions'] + (float) $ta_keywee_sessions : $ta_keywee_sessions;

			$output[$value['parentid']]['superlative'][$post_side]['totals']['Internal']['Pageviews'] =
			isset( $output[$value['parentid']]['superlative'][$post_side]['totals']['Internal']['Pageviews'] ) ? 
			(float) $output[$value['parentid']]['superlative'][$post_side]['totals']['Internal']['Pageviews'] + (float) $ta_internal_pageviews : $ta_internal_pageviews;

			$output[$value['parentid']]['superlative'][$post_side]['totals']['Internal']['Sessions'] =
			isset( $output[$value['parentid']]['superlative'][$post_side]['totals']['Internal']['Sessions'] ) ? 
			(float) $output[$value['parentid']]['superlative'][$post_side]['totals']['Internal']['Sessions'] + (float) $ta_internal_sessions : $ta_internal_sessions;
			
			//Individual posts array - New posts will append to post key in relative superlative
			$output[$value['parentid']]['superlative'][$post_side]['posts'][$post_url_slug] = array(
				'headline' => $post_title_from_slug,
				'pubdate' => $post_pubdate,
				'metrics' => array(
					'GA' => array(
						'Pageviews' => $ga_pageviews,
						'Sessions' => $ga_sessions,
					),
					'MSN' => array(
						'Pageviews' => $msn_pageviews,
						'Sessions' => $msn_sessions,
					),
					'AOL' => array(
						'Pageviews' => $ta_aol_pageviews,
						'Sessions' => $ta_aol_sessions,
					),
					'Keywee' => array(
						'Pageviews' => $ta_keywee_pageviews,
						'Sessions' => $ta_keywee_sessions,
					),
					'Internal' => array(
						'Pageviews' => $ta_internal_pageviews,
						'Sessions' => $ta_internal_sessions,
					),
				),
			);
		
		} //end of foreach

		echo json_encode($output, JSON_PRETTY_PRINT );

	} // end of getTableData()

	// function logout() {
	// 	$this->session->unset_userdata( 'logged_in' );
	// 	session_destroy();
	// 	header( "Location: " . base_url() );
	// } // end logout()

	
}
