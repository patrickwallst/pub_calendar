<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Tools {

	function __construct() {
        // turn off sceen buffering for messages if funning from command line
		if( PHP_SAPI !== 'cli' ) {
        	ob_implicit_flush(true);
        	ob_end_flush();
        }
	} // end __construct

	public function msg( $inText = '', $stop = false, $traceit = false ) {
		if( PHP_SAPI !== 'cli' ) {
			echo "<br>$inText";
			@ob_flush();
			flush();
		} else {
			echo $inText . "\n";
		}
		if( $traceit ) {
			echo '<pre>';
			debug_print_backtrace();
			echo '</pre>';
		}
		if( $stop ) exit( '<br><br>(string) Debug Stop... ');
	} // end msg

	public function msgarray ( $inArray = array(), $stop = false, $traceit = false ) {
		echo '<pre>';
		var_export( $inArray );
		echo '</pre>';
		if( PHP_SAPI !== 'cli' ) {
			@ob_flush();
			flush();
		}
		if( $traceit ) debug_print_backtrace();
		if( $stop ) exit( '<br><br>(array) Debug Stop... ');
	} // end msgarray

   public function message ( $invar = '', $stop = false, $traceit = false ){
   		is_array( $invar ) ? $this->msgarray($invar, $stop) : $this->msg($invar, $stop);
	}

   public function firstSentence( $text ) {
	   	$end = '.?!';
        preg_match("/^[^{$end}]+[{$end}]/", $text, $result);
        return empty($result) ? $text : $result[0];
    } // end firstSentence

    public function split_string( $string, $splitby ) {
    	// $string is any string to parse
    	// $splitby is an array of string delimiters.  Enclose delimiter in () if you want it included in the results.

		$pattern = '/\s?'.implode( $splitby, '\s?|\s?' ).'\s?/m';
		$result = preg_split($pattern, $string, -1,  PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
		return $result;
    } // end split_string()

	 // true if value is present anywhere in multidimensional array
	public function findValue( $array, $field, $elem ) {
	    $top = sizeof($array) - 1;
	    $bottom = 0;
	    while($bottom <= $top) {
	        if(strtolower($array[$bottom][$field]) == strtolower($elem) ) {
	            return true;
	        } elseif( is_array( $array[$bottom][$field] ) ) {
                if( $this->findValue( $array[$bottom][$field], $field, $elem ) ) {
	                return true;
                }
	        }
	        $bottom++;
	    }        
	    return false;
	} // end findValue)()

	public function logit( $msg = '', $logfile = 'log'){
		$msg = date('Y-m-d H:i:s') . ': ' . $msg . "\n";
		$file = "log/{$logfile}_".date('Ymd').'.log';
		file_put_contents($file, $msg, FILE_APPEND | LOCK_EX );
		return;
	}

	public function sendfile( $data, $filename = 'report' ) {
		$arrname = explode('.', $filename );
		$fileName = $arrname[0] . '.csv';

		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header('Content-Description: File Transfer');
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename={$fileName}");
		header("Expires: 0");
		header("Pragma: public");

		$fh = @fopen( 'php://output', 'w' );
		 
		foreach ($data as $row) {
 			fputcsv( $fh, $row );
    	}
 
		// Close the file
		fclose($fh);

		if (file_exists($fileName)) {
			unlink($fileName);
		}

		// Make sure nothing else is sent, our file is done
		exit;
	}

	public function getFile( $url = '' ) {

		$path = parse_url( $url, PHP_URL_PATH );
		$arrurl = explode( '/', $path );
		$filename = end( $arrurl );

		if( !empty( $filename ) ) {
			set_time_limit(0);
			//This is the file where we save the    information
			$fp = fopen ('c:/users/mitch/downloads/' . $filename, 'w+');
			//Here is the file we are downloading, replace spaces with %20
			$ch = curl_init(str_replace(" ","%20",$url));
			curl_setopt($ch, CURLOPT_TIMEOUT, 50);
			// write curl response to file
			curl_setopt($ch, CURLOPT_FILE, $fp); 
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			// get curl response
			curl_exec($ch); 
			curl_close($ch);
			fclose($fp);
		}
	}

	public function savefile( $data, $filename = 'report' ) {
		$arrname = explode('.', $filename );
		$fileName = $arrname[0] . '.csv';

		$fh = @fopen( $filename, 'w' );
		 
		foreach ($data as $row) {
 			fputcsv( $fh, $row );
    	}
 
		// Close the file
		fclose($fh);
	} // end of savefile

	public function secsToTime( $secs ) {
		$hours = floor($secs / 3600);
		$minutes = floor(($secs / 60) % 60);
		// $seconds = $secs % 60 + ( $secs - intval( $secs ) );  // use this one to show factional seconds
		// return sprintf("%02d:%02d:%09.6f", $hours, $minutes, $seconds);
		$seconds = $secs % 60 + round( ( $secs - intval( $secs ) ), 0 );  // use this one to show full seconds only
		return sprintf("%02d:%02d:%02d", $hours, $minutes, $seconds);
	}
	

 public function sendEmail($subject, $body, $email ) {
		require_once APPPATH."third_party/PHPMailer/PHPMailerAutoload.php";
		list( $username, $password ) = explode( '|', file_get_contents( 'data/connections.dat' ) );
		//Create a new PHPMailer instance
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->Host = "ssl://smtp.gmail.com";
		$mail->SMTPAuth = true;
		$mail->Username = $username;
	    $mail->Password = $password;
		$mail->Port = 465;
		$mail->SMTPSecure = 'ssl';
		$mail->SMTPDebug = 0;
		$mail->setFrom('connections@247wallst.com', 'DASHBOARD');
		$mail->addAddress( $email );
		$mail->Subject = $subject;
		$mail->Body = $body;
		$mail->isHTML(true);
		$mail->AltBody = $body;
		$mail->send();	
 }

}