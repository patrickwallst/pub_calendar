<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pubtable_model extends CI_Model{
	var $pubcal = null;
	// var $gawp2 = null;

	public function __construct()
	{
		$this->pubcal = $this->load->database( 'pubcal', TRUE );
		// $this->gawp2 = $this->load->database( 'aboutv2', TRUE );
		$this->pubcal->save_queries = false;
	}

	// function getDate(){
		
	// 	$this->db->where('pubDate >=', $);
	// 	$this->db->where('pubDate <=', $);
	// 	return $this->db->get('orders');
	
	// }

	function getVerticals() {
		$query =<<<Block
select distinct vertical from
post
group by vertical;
Block;

		$res = $this->pubcal->query( $query )->result_array();
		return $res;
	}


	function getSuperlativeDenotation( $vertical, $start_date, $end_date ) {
		$start_date = date('Y-m-d', strtotime($start_date));
		$end_date = date('Y-m-d', strtotime($end_date));

		$query =<<<Block
select a.*, b.msnpageviews, b.msnsessions, parent.side0, parent.side1, parent.entity, parent.universe from
(
SELECT post.*, sum(gadata.pageviews) as gapageviews, sum(gadata.sessions) as sessions
FROM pubcalendar.post 
join gadata using(slug)
where parentid in
(select distinct parentid from post 
where pubDate between '{$start_date}' and '{$end_date}'
and vertical = '{$vertical}')
group by parentid, side, slug
) a 

join 

(
SELECT post.*, sum(msndata.pageviews) as msnpageviews, sum(msndata.uniqueUsers) as msnsessions
FROM pubcalendar.post 
join msndata using(slug)
where parentid in
(select distinct parentid from post 
where pubDate between '{$start_date}' and '{$end_date}'
and vertical = '{$vertical}' )
group by parentid, side, slug
) b
using( slug)
join parent on

a.parentid = parent.parentid
;
Block;

		$res = $this->pubcal->query( $query) ;
		return $res->result_array();

	}


	function getTrafficAcquisitionMetrics( $vertical, $start_month, $end_month ) {

		$query =<<<Block
select 
	a.*, 
	b.msnpageviews, 
	b.msnsessions, 
    parent.side0, 
    parent.side1,
    parent.entity, 
    parent.universe, 
    c.TA_AOL_pageviews,
    c.TA_AOL_sessions,
    d.TA_keywee_pageviews,
    d.TA_keywee_sessions,
    e.TA_internal_pageviews, 
	e.TA_internal_sessions 
from
	( 
	SELECT post.*, sum(gadata.pageviews) as gapageviews, sum(gadata.sessions) as sessions
	FROM pubcalendar.post 
	join 
			gadata 
	using(slug)
	where parentid in
		( 
			select distinct parentid from post 
			where month between {$start_month} and {$end_month}
			and vertical = '{$vertical}'
		 )
	group by parentid, side, slug
	) a 

left join 

	( 
	SELECT post.*, sum(msndata.pageviews) as msnpageviews, sum(msndata.uniqueUsers) as msnsessions
	FROM pubcalendar.post 
	join msndata using(slug)
	where parentid in
	( 
		select distinct parentid from post 
		where month between {$start_month} and {$end_month}
		and vertical = '{$vertical}'
	 )
	group by parentid, side, slug 
	) b

 using(slug)


 left join parent 
 on
  a.parentid = parent.parentid
  
left join 
  
( 
	SELECT gadata.slug, sum(gadata.pageviews) as TA_AOL_pageviews, sum(gadata.sessions) as TA_AOL_sessions 
	from pubcalendar.gadata
	where source = "AOL" and 
	campaign = "AOL"
	group by slug
) c 

on
c.slug = a.slug

left join 
  
( 
	SELECT gadata.slug, sum(gadata.pageviews) as `TA_keywee_pageviews`, sum(gadata.sessions) as `TA_keywee_sessions`
	from pubcalendar.gadata
	where ((source = "Facebook" and campaign = "keywee") or (source = "Keywee" and campaign = "keywee") ) 
	group by slug
) d 

on
d.slug = a.slug

left join 
  
( 
	SELECT gadata.slug, sum(gadata.pageviews) as `TA_internal_pageviews`, sum(gadata.sessions) as `TA_internal_sessions`
	from pubcalendar.gadata
	where ((source = "Facebook" and campaign = "247") or (source = "Facebook" and campaign = "internal") ) 
	group by slug
) e

on
e.slug = a.slug;

Block;

		$res = $this->pubcal->query( $query) ;
		//$this->tools->message($this->pubcal->last_query());
		return $res->result_array();
	}

}