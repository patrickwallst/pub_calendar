<div id="app" style="width: 100%;margin-top: 100px;">
    <v-table :data="parentArticles" :button-names="buttonArray">     
    </v-table>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/vue.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/exportToCsv.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/table.js'); ?>"></script>

</body>