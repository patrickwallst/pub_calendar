<div class="filterBar">
	
	<div class="filterSet">
		<span>Month and Verticle</span>
		<div>
			<select class="filter" id="month-select" name="month-select">
                <option value="January">January</option>
                <option value="Febuary">Febuary</option>
                <option value="March">March</option>
                <option value="April">April</option>
                <option value="May">May</option>
                <option value="June">June</option>
                <option value="July">July</option>
                <option value="August">August</option>
                <option value="September">September</option>
                <option value="October">October</option>
                <option value="November">November</option>
                <option value="December">December</option>
            </select>
            <select class="filter" id="verticle-select-1" name="verticle-select-1">
                <?php foreach ($verticals as $row) { echo '<option value="' . $row . '">' . $row . '</option>'; }?>
            </select>
			<!-- <button class="filter">This week</button> -->
		</div>
    </div>
	
	<div class="status">
        <span>Timeframe</span>
        <div>
            <select class="filter" id="verticle-select-2" name="verticle-select-2">
                <?php foreach ($verticals as $row) { echo '<option value="' . $row . '">' . $row . '</option>'; }?>
            </select>
            <label for="dateofbirth">Start Date</label>
            <select class="filter" name="start-date" id="start-date">
                <option value="1">January</option>
                <option value="2">Febuary</option>
                <option value="3">March</option>
                <option value="4">April</option>
                <option value="5">May</option>
                <option value="6">June</option>
                <option value="7">July</option>
                <option value="8">August</option>
                <option value="9">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
            </select>
            <label for="dateofbirth">End Date</label>
            <select class="filter" name="end-date" id="end-date">
                <option value="1">January</option>
                <option value="2">Febuary</option>
                <option value="3">March</option>
                <option value="4">April</option>
                <option value="5">May</option>
                <option value="6">June</option>
                <option value="7">July</option>
                <option value="8">August</option>
                <option value="9">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
            </select>

            <button id="filter-call" class="button-filter"><span>Filter</span></button>
        </div>
	</div>
	
</div>

<div id='loading-ani' class="boxLoading" style="display: none;">
</div>

<script>
// Hide .filterBar fixed div on scroll down
(function (document, window, index) {

    var elSelector = '.filterBar',
        element = document.querySelector(elSelector);

    if (!element) return true;

    var elHeight = 0,
        elTop = 0,
        dHeight = 0,
        wHeight = 0,
        wScrollCurrent = 0,
        wScrollBefore = 0,
        wScrollDiff = 0;

    window.addEventListener('scroll', function () {
        elHeight = element.offsetHeight;
        dHeight = document.body.offsetHeight;
        wHeight = window.innerHeight;
        wScrollCurrent = window.pageYOffset;
        wScrollDiff = wScrollBefore - wScrollCurrent;
        elTop = parseInt(window.getComputedStyle(element).getPropertyValue('top')) + wScrollDiff;

        if (wScrollCurrent <= 0) // scrolled to the very top; element sticks to the top
            element.style.top = '0px';

        else if (wScrollDiff > 0) // scrolled up; element slides in
            element.style.top = (elTop > 0 ? 0 : elTop) + 'px';

        else if (wScrollDiff < 0) // scrolled down
        {
            if (wScrollCurrent + wHeight >= dHeight - elHeight)  // scrolled to the very bottom; element slides in
                element.style.top = ((elTop = wScrollCurrent + wHeight - dHeight) < 0 ? elTop : 0) + 'px';

            else // scrolled down; element slides out
                element.style.top = (Math.abs(elTop) > elHeight ? -elHeight : elTop) + 'px';
        }

        wScrollBefore = wScrollCurrent;
    });

} (document, window, 0));

</script>