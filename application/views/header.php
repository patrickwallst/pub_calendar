<?php
require FCPATH . 'vendor/leafo/scssphp/scss.inc.php';
$scss = new scssc();
$scss->setImportPaths("assets/stylesheets/");
$scss_ready = $scss->compile( '@import "style.scss"' );
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">

	<title>Publication Table</title>

    <style><?php echo $scss_ready; ?></style>
  

</head>
<body>