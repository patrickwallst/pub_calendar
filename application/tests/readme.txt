PHPunit and Testing

PHPunit has been installed via Composer in our project. 

To run your test (in powershell or bash) just run the following command 
replacing the “yourTestClass” with the name of your test file:

./vendor/bin/phpunit application/tests

From Steven: This is how I can run the entire test suite. Notice the backslashes instead of forward slashes because windows environment vendor\bin\phpunit application\tests

Since install, the directories where PHPunit is looking for classes 
to test are "application/controllers", "application/models", and "application/views”.

To add more directories, go to your composer.json file and add more values under 
autoload->classmap object. 
After saving your composer.json file, run composer update from the base directory.

*************************************
Had to add a bit of a 'magic' to make codeigniter work seamlessly with phpunit.
Can see the starting doc for those changes here -> https://github.com/fmalk/codeigniter-phpunit