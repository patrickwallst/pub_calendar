var buttonArr = {
    'GA': 'site',
    'MSN': 'site',
    'AOL': 'site',
    'Keywee': 'site',
    'Internal': 'site',
    'Pageviews': 'metric',
    'Sessions': 'metric',
};

Vue.component('v-table', {
    props: {
        data: Object,
        buttonNames: Object
    },
    template: `
    <div>
    <div class="table-users" v-for="parent in data">
        <div class="table-header">{{ parent.parentName }}</div>
        <button v-for="(val, key) in buttonNames" class="button-filter" v-on:click="myFilter($event, key, val)">{{ key }}</button>
        <table cellspacing="0">
            <thead>
                <tr>
                    <th data-site="top" data-metric="headline">Headline</th>
                    <th data-site="top" data-metric="pubdate">Pubdate</th>
                    <template v-for="(total, key) in parent.totals">
                        <th v-for="(totalMetric, metricName) in total" :data-site="key" :data-metric="metricName">{{ key + ' ' + metricName }}</th>
                    </template>
                </tr>
            </thead>
            <tbody>
                <tr class='total-table-row'>
                    <td>Totals</td>
                    <td>{{ parent.pubdate }}</td>
                    <template v-for="(total, key) in parent.totals">
                        <td v-for="(totalMetric, metricName) in total" :data-site="key" :data-metric="metricName">{{ totalMetric }}</td>
                    </template>
                </tr>
                <template v-for="sides in parent.superlative">
                    <template v-if="sides.pubdate !== ''">
                        <tr class='total-table-row' v-on:click="accordionTable($event, sides.side)">
                            <td>{{ sides.side }}</td>
                            <td>{{ naIfBlank( sides.pubdate ) }}</td>
                            <template v-for="(total, key) in sides.totals">
                                <td v-for="(totalMetric, metricName) in total" :data-site="key" :data-metric="metricName">{{ naIfBlank( totalMetric ) }}</td>
                            </template>
                        </tr>
                        <tr :data-side="sides.side" v-for="posts in sides.posts">
                            <td>{{ naIfBlank( posts.headline ) }}</td>
                            <td>{{ naIfBlank( posts.pubdate ) }}</td>
                            <template v-for="(metric, key) in posts.metrics">
                                <td v-for="(totalMetric, metricName) in metric" :data-site="key" :data-metric="metricName">{{ naIfBlank( totalMetric ) }}</td>
                            </template>
                        </tr>
                    </template>    
                </template>
            </tbody>
        </table>
        <div class="bottom-button-div">
            <div>
                <span>Priority</span>
                <div class='bottom-button-container' data-id='post-priority'>
                    <button class="bottom-buttons single-sel" v-on:click="selectedShow($event)">1</button>
                    <button class="bottom-buttons single-sel" v-on:click="selectedShow($event)">2</button>
                    <button class="bottom-buttons single-sel" v-on:click="selectedShow($event)">3</button>
                </div>
            </div>
            <div>
                <span>Choose A Superlative</span>
                <div class='bottom-button-container' data-id='post-superlative'>
                    <button v-for="(sides, key) in parent.superlative" :data-id="key" class="bottom-buttons" v-on:click="superlativeShow($event, key, sides.side)">{{ sides.side }}</button>
                    <button class="bottom-buttons single-sel combo-buttons" style="display: none;" v-on:click="selectedShow($event)">Combined</button>
                    <button class="bottom-buttons single-sel combo-buttons" style="display: none;" v-on:click="selectedShow($event)">Separate</button>
                </div>
            </div>
        </div>
    </div>
    <div id="save-button-div" style='display: none;'>
        <div>
            <span>Any Articles without Priority selected will not be added to CSV:</span>
            <div class='save-button-container' data-id='save'>
                <button id="save-button" class="save-buttons" v-on:click="saveToCsv()"><span>Finish and Save</span></button>
            </div>
        </div>
    </div>
    </div>
  `,
    methods: {
        parseKey: function (keyName, type) {
            //type parameter is the array pointer
            var parse = keyName.split('-');
            return parse[type];
        },
        myFilter: function (event, metric, type) {
            let colsArray = event.srcElement.parentNode.querySelectorAll(' [data-' + type + '=\"' + metric + '\"]'); //test that this equals array
            if (event.srcElement.classList.contains("active")) {
                event.srcElement.classList.remove("active");
                colsArray.forEach(function (item, i) {
                    item.style.display = '';
                });
            }
            else {
                event.srcElement.classList.add("active");
                colsArray.forEach(function (item, i) {
                    item.style.display = 'none';
                });
            }
        },
        superlativeShow: function (event, key, side) { //will change to not having to double click
            event.srcElement.classList.toggle("selected-super");
            let superSelectArray = event.srcElement.parentNode.querySelectorAll('button'); //test that this equals array
            let superLength = superSelectArray.length;
            let selectedLength = event.srcElement.parentNode.querySelectorAll('.selected-super').length;

            let buttonsOn = event.srcElement.parentNode.querySelectorAll('button');
            buttonsOn.forEach(function (item, i) {
                if (item.classList.contains("selected-super") && event.srcElement !== item) {
                    item.classList.remove("selected-super");
                }
            });

            if (key === 2 && event.srcElement.classList.contains("selected-super")) {
                let comboSeparate = event.srcElement.parentNode.querySelectorAll('.combo-buttons');
                comboSeparate.forEach(function (item, i) {
                    item.style.display = '';
                });
            } else {
                let comboSeparate = event.srcElement.parentNode.querySelectorAll('.combo-buttons');
                comboSeparate.forEach(function (item, i) {
                    item.style.display = 'none';
                });
            }
        },
        selectedShow: function (event) {
            event.srcElement.classList.toggle("selected-single");
            let buttonsOn = event.srcElement.parentNode.querySelectorAll('button');
            buttonsOn.forEach(function (item, i) {
                if (item.classList.contains("selected-single") && event.srcElement !== item) {
                    item.classList.remove("selected-single");
                }
            });
        },
        naIfBlank: function (value) {
            if(value === '') {
                return 'N/A';
            } else {
                return value;
            }
        },
        hideRowIfBlank: function (value) {
            if (value === '') {
                return 'display: none;';
            } else {
                return '';
            }
        },
        saveToCsv: function () {
            let finalArr = new Array();
            let monthSelection = document.getElementById('month-select').value;
            let verticalSelection = document.getElementById('verticle-select-1').value;
            let fileName = monthSelection + '_' + verticalSelection;
            let parentDivs = document.querySelectorAll('.table-users');
            parentDivs.forEach(function ( item ) {
                let parentName = item.querySelector('.table-header').innerText;
                let priorityValue = '';
                let comboChoice = '';
                let superChoice = '';
                if ( item.querySelector('[data-id="post-priority"]').querySelector('.selected-single') != null ) {
                    priorityValue = item.querySelector('[data-id="post-priority"]').querySelector('.selected-single').innerText;
                } else { 
                    return; // If priority not selected skip parent article
                }
                let superDiv = item.querySelector('[data-id="post-superlative"]').querySelectorAll('button');
                let superDivCount = superDiv.length;
                superDiv.forEach(function (item2, index) {
                    if ( item2.classList.contains("selected-super") ) {
                        if ( item2.getAttribute('data-id') == 2 ) {
                            superChoice = item2.innerText;
                            if ( item2.parentNode.querySelector('.selected-single') != null ) {
                                comboChoice = item2.parentNode.querySelector('.selected-single').innerText;
                            } else {
                                // If combination or separate selected but everything else is,
                                // stop function and alert user to select a value
                                alert('No Selection for [Combined] or [Serparate] at ' + parentName + ' -not added to CSV');
                                return; 
                            }
                        } else {
                            superChoice = item2.innerText;
                            comboChoice = '.';
                        }
                        let parentObjectSelections = {
                            Vertical: verticalSelection,
                            Month: monthSelection,
                            Parent: parentName,
                            Priority: priorityValue,
                            Superlative_Choice: superChoice,
                            Combination_or_Separate: comboChoice
                        };
                        finalArr.push(parentObjectSelections);
                    }
                })
            }); // end of foreach
            downloadCSV({ filename: fileName, data: finalArr });
            // console.log(finalArr); 
        },
        accordionTable: function( event, side ) {
            let sideSelectArray = event.srcElement.parentNode.parentNode.querySelectorAll('[data-side="' + side + '"]'); 
            sideSelectArray.forEach(function (item, i) {
                item.classList.toggle('hide-tr');
            });
        }
    }
})

var vm = new Vue({
    el: "#app",
    data: {
        parentArticles: null,
        buttonArray: buttonArr
    },
    methods: {
        fetchData: function () {
            var xhr = new XMLHttpRequest();
            var self = this;
            var vertical = document.querySelector('[name="verticle-select-2"]').value;
            var startDate = document.querySelector('[name="start-date"]').value;
            var endDate = document.querySelector('[name="end-date"]').value;
            xhr.open(
                "GET",
                "Pubtable/getTableData/" + encodeURIComponent(vertical) + "/" + startDate + "/" + endDate
            );
            xhr.responseType = 'json';
            xhr.onload = function () {
                if (xhr.status >= 200 && xhr.status < 400) {
                    // Success!
                    // var data = JSON.parse(xhr.responseText);
                    self.parentArticles = xhr.response;
                    document.getElementById('save-button-div').style.display = 'flex';
                    document.getElementById('loading-ani').style.display = 'none';
                } else {
                    // We reached our target server, but it returned an error
                }
            };
            xhr.onerror = function () {
                // There was a connection error of some sort
            };
            xhr.send();
        }
    }
});

// Filter Function Event 
document.querySelector('#filter-call').addEventListener('click', function () {
    vm.fetchData();
    document.getElementById('loading-ani').style.display = '';
});