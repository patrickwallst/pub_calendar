For testing our Javascript assets, we’ll use Jest. It is a unit testing library created by Facebook. 
It has become extremely popular as it is very much a swiss army knife for Javascript testing. 
Being created by Facebook means frequent updates from some of the best JS devs in the world.

That said getting Jest up and running on your local will take some time if you’ve never used NPM. 
First thing to do is to get NPM for windows (https://nodejs.org/en/download/). 
NPM runs from the CLI by typing “npm” followed by the command.

While the necessary npm, yarn, and jest modules are already installed in our projects repo, 
it will save some frustration to at least install “Yarn” as a global CLI command. To do that simply enter into the CLI

-> npm install -g yarn

You can find the tests folder in the “assets/js” directory in our project. 
To run a js test simply run 

-> yarn test testExample.test.js 

With testExample.test.js being the name of the test. 
In the Js and js/test folder there is an example test and test file that you can refer to for help!
